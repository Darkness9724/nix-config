# Usage

```
# nixos-rebuild switch --flake git+https://gitlab.com/Darkness9724/nix-config
```

OR

```
$ git clone https://gitlab.com/Darkness9724/nix-config
$ cd nix-config
# nixos-rebuild switch --flake .
```
