{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.supportedFilesystems = [ "exfat" "ntfs" ];
  boot.kernelPackages = pkgs.linuxPackages_xanmod;
  boot.kernelModules = [ "kvm-amd" "pcspkr" "winesync" ];
  boot.kernelParams = [
    "amd_pstate=passive"
    "lsm=integrity,capability,landlock,yama,apparmor,bpf,safesetid"
    "mitigations=off"
    # Fuck so called "predictable names", it's total mess
    "net.ifnames=0"
    # Disable AMD IOMMU and enable software IOMMU for some PCI-E cards
    "amd_iommu=off"
    "iommu=soft"
  ];
  boot.kernel.sysctl = {
    "vm.dirty_bytes" = 134200000;
    "vm.dirty_background_bytes" = 67100000;
    "vm.overcommit_memory" = 1;
    "vm.overcommit_ratio" = 100;
    "vm.swappiness" = 20;
    "kernel.sysrq" = 1;
  };
  boot.extraModulePackages = [ ];

  boot.plymouth = {
    enable = true;
    theme = "breeze";
    extraConfig = "DeviceScale=2";
  };

  boot.bootspec.enable = true;
  boot.loader = {
    systemd-boot.enable = true;
    systemd-boot.configurationLimit = 2;
    efi.canTouchEfiVariables = true;
  };

  boot.initrd = {
    availableKernelModules = [ "nvme" "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" "bcache" ];
    kernelModules = [ "amdgpu" ];

    luks.devices = {
      "root".device = "/dev/disk/by-uuid/727603fc-2445-4735-8467-7323ada52d60";
      "storage".device = "/dev/disk/by-uuid/17b18140-1f39-4bf7-96c3-afcac663dea4";
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/mapper/root";
      fsType = "btrfs";
      options = [ "subvol=@nixos" "compress-force=zstd" ];
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/FDC3-924C";
      fsType = "vfat";
    };

    "/home/darkness9724/Applications/MGQverse" = {
      device = "/dev/disk/by-uuid/1a44fa86-ef84-4d2e-b530-31be13680c00";
      fsType = "btrfs";
      options = [ "subvol=@mgq" ];
    };

    # Arch Linux
    "/mnt" = {
      device = "/dev/disk/by-uuid/1a44fa86-ef84-4d2e-b530-31be13680c00";
      fsType = "btrfs";
      options = [ "subvol=@" "compress-force=zstd" ];
    };

    "/mnt/home" = {
      device = "/dev/disk/by-uuid/1a44fa86-ef84-4d2e-b530-31be13680c00";
      fsType = "btrfs";
      options = [ "subvol=@home" "compress-force=zstd" ];
    };

    # Bind the various directories from the Arch subvolume
    "/home/darkness9724/.local/share/Steam" = {
      device = "/mnt/home/darkness9724/.local/share/Steam";
      fsType = "none";
      options = [ "bind" ];
    };

    "/home/darkness9724/Steam" = {
      device = "/mnt/home/darkness9724/Steam";
      fsType = "none";
      options = [ "bind" ];
    };

    "/home/darkness9724/.steam" = {
      device = "/mnt/home/darkness9724/.steam";
      fsType = "none";
      options = [ "bind" ];
    };

    "/home/darkness9724/.gnupg" = {
      device = "/mnt/home/darkness9724/.local/share/gnupg";
      fsType = "none";
      options = [ "bind" ];
    };

    "/home/darkness9724/.config/git" = {
      device = "/mnt/home/darkness9724/.config/git";
      fsType = "none";
      options = [ "bind" ];
    };

    "/home/darkness9724/.ssh" = {
      device = "/mnt/home/darkness9724/.ssh";
      fsType = "none";
      options = [ "bind" ];
    };

    # The second disk
    "/storage" = {
      device = "/dev/disk/by-uuid/536f52df-3664-4fe1-9e78-1ced372ae469";
      fsType = "btrfs";
    };

    "/tmp" = {
      device = "tmpfs";
      fsType = "tmpfs";
    };

    # Access to a consistently clean tmpfs without system clutter
    "/ram" = {
      device = "tmpfs";
      fsType = "tmpfs";
    };
  };

  swapDevices = [ ];

  hardware.opengl.extraPackages = with pkgs; [
    rocm-opencl-icd
    rocm-opencl-runtime
  ];
  hardware.bluetooth.enable = true;
  hardware.keyboard.qmk.enable = true;

  # Let Xanmod kernel control the CPU frequency and power consumption
  powerManagement.cpuFreqGovernor = "schedutil";

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp5s0.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
