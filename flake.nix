{
  description = "Darkness9724's flaky";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager/release-23.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    chaotic.url = "github:chaotic-cx/nyx/nyxpkgs-unstable";
    nur.url = "github:nix-community/NUR";
    nixified.url = "github:nixified-ai/flake";
    nix-index-database.url = "github:nix-community/nix-index-database";
    nix-index-database.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
    self,
    nixpkgs,
    home-manager,
    chaotic,
    unstable,
    nur,
    nixified,
    nix-index-database,
    ...
  }@inputs:
  let
    inherit (self) outputs;

    forAllSystems = nixpkgs.lib.genAttrs [
      "x86_64-linux"
    ];
    forAllPackages = pkgs: forAllSystems (system: pkgs nixpkgs.legacyPackages.${system});

  in {
    nixosModules = import ./modules/nixos;
    homeManagerModules = import ./modules/home-manager;

    overlays = import ./overlays { inherit inputs; };
    packages = forAllPackages (pkgs: import ./pkgs {inherit pkgs;});
    devShells = forAllPackages (pkgs: import ./shell.nix { inherit pkgs; });

    nixosConfigurations = {
      desktop = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = { inherit inputs outputs; };
        modules = [
            ({ pkgs, ...} : {
                nix.registry = {
                    nixpkgs.flake = nixpkgs;
                    unstable.flake = unstable;
                    chaotic.flake = chaotic;
                    nixified.flake = nixified;
                };
            })
          ./desktop/configuration.nix
          home-manager.nixosModules.home-manager {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.darkness9724 = import ./desktop/home.nix;
          }
          chaotic.nixosModules.default
          nur.nixosModules.nur
          nix-index-database.nixosModules.nix-index
        ];
      };
    };

    laptop = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = { inherit inputs outputs; };
      modules = [
          ({ pkgs, ...} : {
              nix.registry = {
                  nixpkgs.flake = nixpkgs;
                  unstable.flake = unstable;
                  chaotic.flake = chaotic;
                  nixified.flake = nixified;
              };
          })
        ./laptop/configuration.nix
        home-manager.nixosModules.home-manager {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.users.darkness9724 = import ./laptop/home.nix;
        }
        chaotic.nixosModules.default
        nur.nixosModules.nur
      ];
    };
  };
}
