{ inputs, outputs, config, pkgs, lib, home-manager, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      outputs.nixosModules.tinyproxy-impure
      outputs.nixosModules.shadowsocks-rust
    ];

  # Nix and Nixpkgs
  nix.settings = {
    auto-optimise-store = true;
    experimental-features = [ "nix-command" "flakes" ];
    trusted-users = [ "darkness9724" ];
  };

  nixpkgs = {
    overlays = [
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.unstable-packages
      inputs.nur.overlay
    ];

    config = {
      allowUnfree = true;
      allowBroken = true;
    };
  };

  # System
  time.timeZone = "Europe/Paris";

  i18n.defaultLocale = "en_US.UTF-8";

  console = {
    earlySetup = true;
    font = "ter-v18n";
    packages = [ pkgs.terminus_font ];
    useXkbConfig = true;
  };

  # Users
  users.users.darkness9724 = {
    isNormalUser = true;
    extraGroups = [ "wheel" "adbusers" ];
    shell = pkgs.fish;
  };

  # Network
  networking.hostName = "laptop";
  networking.networkmanager.enable = true;
  networking.firewall = {
    enable = true;
    allowedTCPPortRanges = [
            { from = 1714; to = 1764; }
    ];
    allowedUDPPortRanges = [
            { from = 1714; to = 1764; }
    ];
  };
  networking.nameservers = [ "127.0.0.1" ];
  networking.networkmanager.dns = "none";
  # To be safe
  networking.hosts = {
    "127.0.0.1" = [ "files.ea.com" ];
  };

  # Services
  services.getty.autologinUser = "darkness9724";
  services.fstrim.enable = true;

  services.openssh.enable = true;

  services.dnscrypt-proxy2 = {
    enable = true;
    settings = {
      ipv6_servers = false;
      require_dnssec = true;

      sources.public-resolvers = {
        urls = [
            "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/opennic.md"
            "https://download.dnscrypt.info/resolvers-list/v3/opennic.md"
        ];
        cache_file = "/var/lib/dnscrypt-proxy2/opennic.md";
        minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
      };
    };
  };
  systemd.services.dnscrypt-proxy2.serviceConfig.StateDirectory = "dnscrypt-proxy";

  systemd.services.NetworkManager-wait-online = {
    serviceConfig = {
      ExecStart = [ "" "${pkgs.networkmanager}/bin/nm-online -q" ];
    };
  };

  services.tinyproxy-impure = {
    enable = true;
    config = "/etc/tinyproxy/tinyproxy.conf";
  };

  services.shadowsocks_rust = {
    enable = true;
    config = "/etc/shadowsocks/config.json";
  };

  services.i2pd = {
    enable = false;
    proto.http.enable = true;
    proto.httpProxy.enable = true;
  };

  services.tor = {
    enable = true;
    client.enable = true;
    settings = {
      ExcludeExitNodes = ["fr"];
    };
  };

  services.yggdrasil = {
    enable = true;
    configFile = "/etc/yggdrasil.conf";
  };

  # GUI
  services.xserver = {
    enable = true;
    xkbModel = "pc105";
    layout = "us,ru";
    displayManager = {
      startx.enable = true;
      defaultSession = "plasmawayland";
    };
    desktopManager.plasma5.enable = true;
    libinput.enable = true;
  };

  environment.plasma5.excludePackages = with pkgs.libsForQt5; [
    elisa
    oxygen
    kwrited
  ];

  # Packages and programs
  environment.systemPackages = with pkgs; [
    # Control CPU
    linuxKernel.packages.linux_xanmod.cpupower
    # Almost best console editor
    micro
    # Command-line downloader
    wget
    # Command-line process manager
    htop
    # Command-line I/O viewer
    iotop-c
    # A faster and simpler alternative to find
    fd
    # A faster alternative to grep
    ripgrep
    # GUI partition manager
    gparted
    # Hardware sensors
    lm_sensors
    # Disk SMART
    smartmontools
    # Gamer moment
    proton-ge-custom
    luxtorpeda
    # Utility for copypasting under Wayland
    wl-clipboard
    # Wayland alternative to xdotool
    ydotool
    # Utility to upscale images using ESRGAN
    realesrgan-ncnn-vulkan
    # Command-line image manipulation utility
    imagemagick
    # Just for notify-send command
    libnotify
    # Simple command-line calculator
    calc
    # Utility to view memory usage of all process
    ps_mem
    # Manipulation utility for 7z archives
    p7zip
    # ZIP archives packer
    zip
    # ZIP archives unpacker
    unzip
    # Appimage runner
    appimage-run
    # Utility to manipulate exif metadata
    exiv2
    # Command-line media manipulation utility
    ffmpeg
    # Utility which can run an external program in the proton prefix
    nur.repos.ataraxiasjel.protonhax
    # Modern du replacement
    du-dust
    # Modern df replacement
    lsd
    # btrfs file size measurement
    compsize
    # Monero
    monero-cli
    # ALSA userspace tools
    alsa-utils
    # for kinfocenter
    pciutils
    fwupd
    mesa-demos
    wayland-utils
    vulkan-tools
    clinfo
    # Android
    adbfs-rootless
    gnirehtet

    # Gstreamer
    gst_all_1.gstreamer.bin
    gst_all_1.gstreamer.out
    gst_all_1.gst-vaapi
    gst_all_1.gst-plugins-ugly
    gst_all_1.gst-plugins-bad
    gst_all_1.gst-plugins-good
    gst_all_1.gst-plugins-rs
    gst_all_1.gst-plugins-base
    gst_all_1.gst-libav
  ];

  programs.fish.enable = true;
  programs.git.enable = true;
  programs.ccache.enable = true;
  programs.steam.enable = true;
  programs.steam.gamescopeSession.enable = true;
  programs.gamescope = {
    enable = true;
    package = pkgs.gamescope;
    args = [ "-f" "-U" "-w 1920" "-h 1080" "-r 60" ];
  };

  chaotic.steam.extraCompatPackages = with pkgs; [
    proton-ge-custom
  ];

  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  programs.dconf.enable = true;

  programs.partition-manager.enable = true;
  programs.cdemu.enable = true;
  programs.adb.enable = true;
  programs.firejail.enable = true;

  environment.shellAliases = {
  	code = "codium";
  };

  environment.variables = {
    EDITOR = "micro";
  };

  zramSwap.enable = true;
  zramSwap.algorithm = "lz4hc";

  security.sudo.extraRules = [
  	{
  		groups = [ "wheel" ];
  		commands = [
  			{
  				command = "ALL";
  				options = [ "NOPASSWD" ];
  			}
  		];
  	}
  ];

  security.rtkit.enable = true;
  services.pipewire = {
        enable = true;
        # For full compatibility
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
        jack.enable = true;
  };

  fonts = {
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      libertinus
      paratype-pt-sans
      paratype-pt-serif
      paratype-pt-mono
    ];
    fontDir.enable = true;
    fontconfig.enable = true;
  };

  system.copySystemConfiguration = false;
  system.stateVersion = "23.11";
}
