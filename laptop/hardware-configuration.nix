{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.supportedFilesystems = [ "exfat" ];
  boot.kernelPackages = pkgs.linuxPackages_xanmod;
  boot.kernelModules = [ "kvm-amd" ];
  boot.kernelParams = [
    "amd_pstate=passive"
    "lsm=integrity,capability,landlock,yama,apparmor,bpf,safesetid"
    "mitigations=off"
  ];
  boot.kernel.sysctl = {
    "vm.dirty_bytes" = 134200000;
    "vm.dirty_background_bytes" = 67100000;
    "vm.overcommit_memory" = 1;
    "vm.overcommit_ratio" = 100;
    "vm.swappiness" = 20;
    "kernel.sysrq" = 1;
  };
  boot.extraModulePackages = [ ];

  boot.plymouth = {
    enable = true;
    theme = "breeze";
    extraConfig = "DeviceScale=2";
  };

  boot.bootspec.enable = true;
  boot.loader = {
    systemd-boot.enable = true;
    systemd-boot.configurationLimit = 2;
    efi.canTouchEfiVariables = true;
  };

  boot.initrd = {
    availableKernelModules = [ "nvme" "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" "bcache" ];
    kernelModules = [ "amdgpu" ];

    luks.devices = {
      "root".device = "/dev/disk/by-uuid/62c7fe28-fe10-4d8f-9891-55be15b218ee";
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/3e4b170e-1d1e-40ec-9d76-5f9775021dc4";
      fsType = "btrfs";
      options = [ "subvol=@" "compress-force=zstd" ];
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/99C2-EA6E";
      fsType = "vfat";
    };

    "/home" = {
      device = "/dev/disk/by-uuid/3e4b170e-1d1e-40ec-9d76-5f9775021dc4";
      fsType = "btrfs";
      options = [ "subvol=@home" "compress-force=zstd" ];
    };
  };

  swapDevices = [ ];

  hardware.opengl.extraPackages = with pkgs; [
    rocm-opencl-icd
    rocm-opencl-runtime
  ];
  hardware.bluetooth.enable = true;

  # Laptop should run as long as possible on battery power
  powerManagement.cpuFreqGovernor = "powersave";

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp1s0.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp2s0.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
