{ outputs, config, pkgs, ... }:

{
  home.stateVersion = "23.11";

  home.packages = with pkgs; [
    # Password manager
    keepassxc
    # Browser
    firedragon
    # VN development
    renpy
    # For some applications that I run outside of Steam Proton/Bottles
    wineWowPackages.wayland
    font-manager
    # Music player
    audacious
    audacious-plugins
    # IM
    whatsapp-for-linux
    # Text editor
    kate
    # Torrent client
    qbittorrent
    # Image Editor
    gimp-with-plugins
    # Timer
    ktimer
    # Vector editor
    inkscape
    # Maple alternative
    wxmaxima
    # MATLAB alternative
    octave
    # IDE for R
    rstudio
    # Notebook, cuz Emacs sucks
    logseq
    # Markdown editor
    ghostwriter
    # Cryptowallets
    monero-gui
    electrum
    # Office suite
    onlyoffice-bin_latest
    retroshare
    keybase-gui
    # Bibliography manager
    zotero
    qnotero
    # Cheat Engine alternative
    scanmem

    # Dependencies
    kdialog

    # Rebuild
    dolphin
    spectacle
  ];

  xdg = {
    enable = true;
    cacheHome = "${config.home.homeDirectory}/.cache";
    configHome = "${config.home.homeDirectory}/.config";
    dataHome = "${config.home.homeDirectory}/.local/share";
    stateHome = "${config.home.homeDirectory}/.local/state";
  };

  home.sessionVariables = {
    # Because both vim and nano suck
    EDITOR = "micro";
    # Just to ensure
    GST_VAAPI_ALL_DRIVERS = 1;
    # For pytorch
    HSA_OVERRIDE_GFX_VERSION = "10.3.0";
    # Prevent Wine from messing up with file associations
    WINEDLLOVERRIDES = "winemenubuilder.exe=d";
  };

  programs.fish = {
    enable = true;
    shellAbbrs = {
        codium = "code";
    };
    # Instead of using sddm or something like that
    loginShellInit = ''
      if test -z "$DISPLAY" -a "$XDG_VTNR" = 1 -a "$XDG_SESSION_TYPE" = "tty"
        XDG_SESSION_TYPE=wayland dbus-run-session startplasma-wayland
      end
    '';
  };

  programs.texlive = {
        enable = false;
        extraPackages = tpkgs: {
                inherit (tpkgs)
                scheme-medium
                scheme-context
                latexmk
                libertinus-otf
                lualatex-math
                anyfontsize
                siunitx
                physics
                wasy
                wasysym
                multirow
                makecell
                svg
                wrapfig
                trimspaces
                transparent
                biblatex
                ccicons
                bigfoot
                ;
        };
  };
  programs.vscode = {
    enable = true;
    enableUpdateCheck = false;
    package = pkgs.vscodium;
    extensions = with pkgs.vscode-extensions; [
        editorconfig.editorconfig
        ryu1kn.partial-diff
        gruntfuggly.todo-tree
        ibm.output-colorizer
        ms-vscode.hexeditor
        james-yu.latex-workshop
        ms-python.python
        ms-python.vscode-pylance
        njpwerner.autodocstring
        adpyke.codesnap
        eamodio.gitlens
    ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
        {
            name = "vscode-diff";
            publisher = "fabiospampinato";
            version = "1.4.2";
            sha256 = "sha256-b1N+m+Y4kUylXrJOU0Y1c9eRI12CSkb5mWyKYy+FAzc=";
        }
        {
            name = "languague-renpy";
            publisher = "LuqueDaniel";
            version = "2.3.6";
            sha256 = "sha256-ubMtLCIs3C8UBrXr1vr3Kqm2K3B8wNlm/THftVyIDug=";
        }
        {
            name = "latex-utilities";
            publisher = "tecosaur";
            version = "0.4.11";
            sha256 = "sha256-KIJuUvcq/SVWq9xapUCq+EV2DRGhWRi3+L/F8XcXzlI=";
        }
        {
            name = "nix-ide";
            publisher = "jnoortheen";
            version = "0.2.2";
            sha256 = "sha256-jwOM+6LnHyCkvhOTVSTUZvgx77jAg6hFCCpBqY8AxIg=";
        }
        {
            name = "hoi4modutilities";
            publisher = "chaofan";
            version = "0.11.2";
            sha256 = "sha256-4vCT6HqE9ezwVJScXdk/Aqv/ZS3oP4U6kcmB1pbuBpE=";
        }
        {
            name = "cwtools-vscode";
            publisher = "tboby";
            version = "0.10.24";
            sha256 = "sha256-1WjKlc2iaDyc32Ga5P2iRUcnL23Fne6n9vj0pRhEY4U=";
        }
    ];
  };
  programs.mangohud = {
        enable = true;
  };
  programs.chromium = {
        enable = true;
        package = pkgs.ungoogled-chromium;
        commandLineArgs = [
          "--ignore-gpu-blocklist"
        ];
  };
  programs.mpv = {
    enable = true;
    bindings = {
        "Alt+RIGHT" = "no-osd cycle video-rotate 90";
        "Alt+LEFT" = "no-osd cycle video-rotate -90";
        "CTRL+1" = "no-osd change-list glsl-shaders set '~~/shaders/FSR.glsl'; show-text 'FSR'";
        "CTRL+2" = "no-osd change-list glsl-shaders set '~~/shaders/FSRCNNX.glsl'; show-text 'FSRCNNX'";
        "CTRL+3" = "no-osd change-list glsl-shaders set '~~/shaders/Anime4K_Clamp_Highlights.glsl:~~/shaders/Anime4K_Restore_CNN_VL.glsl:~~/shaders/Anime4K_Upscale_CNN_x2_VL.glsl:~~/shaders/Anime4K_AutoDownscalePre_x2.glsl:~~/shaders/Anime4K_AutoDownscalePre_x4.glsl:~~/shaders/Anime4K_Upscale_CNN_x2_M.glsl'; show-text 'Anime4k'";
        "CTRL+8" = "no-osd change-list glsl-shaders append '~~/shaders/SSimDownscaler.glsl'; show-text 'SSimDownscaler'";
        "CTRL+9" = "no-osd change-list glsl-shaders append '~~/shaders/KrigBilateral.glsl'; show-text 'KrigBilateral'";
        "CTRL+0" = "no-osd change-list glsl-shaders clr ''; show-text 'Shaders cleared'";
    };
    config = {
        profile = "gpu-hq";
        fs = true;
        vo = "gpu-next";
        hwdec = "vaapi";
        gpu-context = "waylandvk";
        ytdl-format = "bestvideo[height<=?1080]+bestaudio/best";
        linear-downscaling = "no";
        glsl-shader = [
            "~~/shaders/SSimDownscaler.glsl"
            "~~/shaders/KrigBilateral.glsl"
        ];
    };
    profiles = {
        fsrcnnx = {
            glsl-shader = "~~/shaders/FSRCNNX.glsl";
            profile-cond = "math.min(display_width / width, display_height / height) >= 2.0";
        };

        fsr = {
            glsl-shader = "~~/shaders/FSR.glsl";
            profile-cond = "math.min(display_width / width, display_height / height) < 2.0";
        };
    };
    scripts = [ pkgs.mpvScripts.mpris pkgs.mpvScripts.thumbfast pkgs.mpvScripts.uosc ];
  };

  services.easyeffects.enable = true;
  services.kdeconnect.enable = true;
  services.pueue.enable = true;
  services.kbfs.enable = true;
  services.keybase.enable = true;
}
