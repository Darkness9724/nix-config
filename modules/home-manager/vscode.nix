{ outputs, config, pkgs, lib, ... }:

{
  home.activation.boforeCheckLinkTargets = {
    after = [];
    before = [ "checkLinkTargets" ];
    data = ''
      userDir=/home/darkness9724/.config/VSCodium/User
      rm -rf $userDir/settings.json
    '';
  };
  home.activation.afterWriteBoundary = {
    after = [ "writeBoundary" ];
    before = [];
    data = ''
      userDir=/home/darkness9724/.config/VSCodium/User
      rm -rf $userDir/settings.json
      cat \
        ${(pkgs.formats.json {}).generate "blabla"
          config.programs.vscode.userSettings} \
        > $userDir/settings.json
    '';
  };
}
