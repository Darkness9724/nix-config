{ config, pkgs, lib, ... }:

let
  cfg = config.services.shadowsocks-rust;
in

with lib; {
  options = {
    services.shadowsocks-rust = {
      enable = mkOption {
        default = false;
        type = with types; bool;
        description = ''
          Start shadowsocks-rust.
        '';
      };

      config = mkOption {
        default = "/etc/shadowsocks/config.json";
        type = with types; uniq string;
        description = ''
          Path to the config file.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services."shadowsocks-rust" = {
      description = "Shadowsocks-Rust Client Service";
      after = [ "network.service" ];
      wants = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        Type = "simple";
        DynamicUser = "yes";
        NoNewPrivileges = "yes";
        AmbientCapabilities = ["CAP_NET_BIND_SERVICE" "CAP_NET_ADMIN"];
        ExecStart = ''${pkgs.shadowsocks-rust}/bin/ssservice local -c ${cfg.config}'';
      };
      path = [ pkgs.shadowsocks-rust ];
    };

    environment.systemPackages = [ pkgs.shadowsocks-rust ];
  };
}
