{ config, pkgs, lib, ... }:

let
  cfg = config.services.tinyproxy-impure;
in

with lib; {
  options = {
    services.tinyproxy-impure = {
      enable = mkOption {
        default = false;
        type = with types; bool;
        description = ''
          Start tinyproxy.
        '';
      };

      config = mkOption {
        default = "/etc/tinyproxy/tinyproxy.conf";
        type = with types; uniq string;
        description = ''
          Path to the config file.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    users.users.tinyproxy = {
      isSystemUser = true;
      group = "tinyproxy";
    };
    users.groups.tinyproxy = {};
    systemd.services."tinyproxy" = {
      description = "Tinyproxy Web Proxy Server";
      after = [ "network.service" ];
      wants = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        Type = "forking";
        #PIDFile = "/run/tinyproxy/tinyproxy.pid";
        ExecStart = ''${pkgs.tinyproxy}/bin/tinyproxy -c ${cfg.config}'';
        PrivateDevices = "yes";
      };
  };

    environment.systemPackages = [ pkgs.tinyproxy ];
  };
}
