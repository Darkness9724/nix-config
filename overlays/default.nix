{ inputs, ... }:

{
  additions = self: nixpkgs: import ../pkgs {pkgs = self;};

  modifications = self: nixpkgs: {
    # Custom Steam FHS
    steam = nixpkgs.steam.override ({ extraPkgs ? pkgs': [], ... }: {
      extraPkgs = pkgs': (extraPkgs pkgs') ++ (with pkgs'; [
        # for Paradox games
        ncurses6
        # fonts for Unity games
        liberation_ttf
        wqy_microhei
        # for some games
        libkrb5
        keyutils
        # for root access
        polkit
        # mkxp
        libffi
        libyaml
      ]);
    });

    dolphin = nixpkgs.dolphin.overrideAttrs(drv: rec {
      propagatedBuildInputs = drv.propagatedBuildInputs ++ [ nixpkgs.qt5.qtimageformats ];
    });

    spectacle = nixpkgs.spectacle.overrideAttrs(drv: rec {
      buildInputs = drv.buildInputs ++ [ nixpkgs.qt5.qtimageformats ];
    });

    ark = nixpkgs.ark.override {
      unfreeEnableUnrar = true;
    };

    realesrgan-ncnn-vulkan = nixpkgs.realesrgan-ncnn-vulkan.overrideAttrs(drv: rec {
      version = "0.2.0";
      src = nixpkgs.fetchFromGitHub {
        owner = "xinntao";
        repo = drv.pname;
        rev = "v${version}";
        sha256 = "sha256-F+NfkAbk8UtAKzsF42ppPF2UGjK/M6iFfBsRRBbCmcI=";
      };
      models = nixpkgs.fetchzip {
        url = "https://github.com/xinntao/Real-ESRGAN/releases/download/v0.2.5.0/realesrgan-ncnn-vulkan-20220424-ubuntu.zip";
        stripRoot = false;
        sha256 = "sha256-1YiPzv1eGnHrazJFRvl37+C1F2xnoEbN0UQYkxLT+JQ=";
      };
      patches = [
          patches/cmakelists.patch
          patches/models_path.patch
      ];
    });
  };

  unstable-packages = self: nixpkgs: {
    unstable = import inputs.unstable {
      system = self.system;
      config.allowUnfree = true;
    };
  };
}
