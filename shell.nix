{ pkgs }: {
  default = pkgs.mkShell {
    NIX_CONFIG = "experimental-features = nix-command flakes";
    nativeBuildInputs = with pkgs; [
        nix
        git
    ];
  };

  c = pkgs.mkShell {
    NIX_CONFIG = "experimental-features = nix-command flakes";
    LD_LIBRARY_PATH = "${pkgs.stdenv.cc.cc.lib}/lib";
    nativeBuildInputs = with pkgs; [
        nix
        git
        gcc
        cmake
        muon
        ninja
        gnumake
        zlib
        pkg-config
        SDL2
    ];
  };

  python = pkgs.mkShell {
    NIX_CONFIG = "experimental-features = nix-command flakes";
    LD_LIBRARY_PATH = "${pkgs.stdenv.cc.cc.lib}/lib";
    nativeBuildInputs = with pkgs; [
        nix
        git
        python311Packages.pip
        python311Packages.attrs
        python311Packages.psd-tools
        python311Packages.pandas
    ];
  };
}
